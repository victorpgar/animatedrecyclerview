package com.example.victor.animatedrecyclerview.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.victor.animatedrecyclerview.R;
import com.example.victor.animatedrecyclerview.model.Example;

import java.util.ArrayList;

public class ExamplesAdapter extends RecyclerView.Adapter<ExamplesAdapter.ViewHolder> {

    private ArrayList<Example> examples;

    public ExamplesAdapter(ArrayList<Example> examples) {
        this.examples = examples;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Context context = viewGroup.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(R.layout.example_item, viewGroup, false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        Example example = examples.get(i);

        viewHolder.tvNombre.setText(example.getName());
        viewHolder.tvDescripcion.setText(example.getDescription());
        viewHolder.tvCreacion.setText(example.getCreatedAt());
    }

    @Override
    public int getItemCount() {
        return examples.size();
    }

    public void setExamples(ArrayList<Example> examples) {
        this.examples = examples;
        notifyDataSetChanged();
    }

    public void removeItem(int adapterPosition) {
        examples.remove(adapterPosition);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(adapterPosition);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvNombre, tvDescripcion, tvCreacion;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvNombre = itemView.findViewById(R.id.example_item_tvNombre);
            tvDescripcion = itemView.findViewById(R.id.example_item_tvDescripcion);
            tvCreacion = itemView.findViewById(R.id.example_item_tvCreacion);
        }
    }
}


