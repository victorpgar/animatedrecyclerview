package com.example.victor.animatedrecyclerview.model;

public class Example {

    private String name;
    private String description;
    private String createdAt;

    public Example(String aa, String aaa, String aaaa) {
        this.setCreatedAt(aaaa);
        this.setDescription(aaa);
        this.setName(aa);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

}