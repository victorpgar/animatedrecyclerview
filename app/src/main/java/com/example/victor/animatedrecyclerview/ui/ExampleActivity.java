package com.example.victor.animatedrecyclerview.ui;

import android.app.Activity;
import android.content.Context;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.view.inputmethod.InputMethodManager;

import com.example.victor.animatedrecyclerview.R;
import com.example.victor.animatedrecyclerview.adapter.ExamplesAdapter;
import com.example.victor.animatedrecyclerview.model.RecyclerItemTouchHelper;
import com.example.victor.animatedrecyclerview.model.Example;

import java.util.ArrayList;

public class ExampleActivity extends AppCompatActivity implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    private static final String RV_STATE_KEY = "RV_STATE";
    private ArrayList<Example> examples;
    private ExamplesAdapter examplesAdapter;
    private View layout;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView.LayoutManager linearLayoutManager;
    private Parcelable recyclerViewState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_example);

        hideKeyboard(this);

        recyclerView = findViewById(R.id.rvExamples);
        layout = findViewById(R.id.layout);

        examples = new ArrayList<Example>();
        examples.add(new Example("Nombre del ejemplo 1", "Descripcion del ejemplo", "DD/MM/YYYY"));
        examples.add(new Example("Nombre del ejemplo 2", "Descripcion del ejemplo", "DD/MM/YYYY"));
        examples.add(new Example("Nombre del ejemplo 3", "Descripcion del ejemplo", "DD/MM/YYYY"));
        examples.add(new Example("Nombre del ejemplo 4", "Descripcion del ejemplo", "DD/MM/YYYY"));
        examples.add(new Example("Nombre del ejemplo 5", "Descripcion del ejemplo", "DD/MM/YYYY"));
        examples.add(new Example("Nombre del ejemplo 6", "Descripcion del ejemplo", "DD/MM/YYYY"));
        examples.add(new Example("Nombre del ejemplo 7", "Descripcion del ejemplo", "DD/MM/YYYY"));

        examplesAdapter = new ExamplesAdapter(examples);

        recyclerView.setAdapter(examplesAdapter);
        linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        // attaching the touch helper to recycler view
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(recyclerView);

        recyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));

        int resId = R.anim.example_item_animation_layout_from_right;
        LayoutAnimationController animation = AnimationUtils.loadLayoutAnimation(this, resId);
        recyclerView.setLayoutAnimation(animation);

        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.srExamples);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Esto se ejecuta cada vez que se realiza el gesto
                example_onClick(null);
            }
        });

        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
    }

    @Override
    protected void onSaveInstanceState(Bundle state) {
        super.onSaveInstanceState(state);

        // Save list state
        recyclerViewState = linearLayoutManager.onSaveInstanceState();
        state.putParcelable(RV_STATE_KEY, recyclerViewState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);

        // Retrieve list state and list/item positions
        if(state != null)
            recyclerViewState = state.getParcelable(RV_STATE_KEY);
    }

    private void playRefreshAnimation(final RecyclerView recyclerView) {
        final Context context = recyclerView.getContext();
        final LayoutAnimationController controller =
                AnimationUtils.loadLayoutAnimation(context, R.anim.example_item_animation_layout_fall_down);

        recyclerView.setLayoutAnimation(controller);
        recyclerView.getAdapter().notifyDataSetChanged();
        recyclerView.scheduleLayoutAnimation();
    }

    public void example_onClick(View view) {

        hideKeyboard(this);

        examplesAdapter.setExamples(examples);

        playRefreshAnimation(recyclerView);

        if(swipeRefreshLayout.isRefreshing())
            swipeRefreshLayout.setRefreshing(false);
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }

        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof ExamplesAdapter.ViewHolder) {
            // remove the item from recycler view
            examplesAdapter.removeItem(viewHolder.getAdapterPosition());
        }
    }
}